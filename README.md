# daphne

DAPHNE is an instrumentation module for the PDS at DUNE experiment in FERMILAB. DAPHNE is designed to digitize the analog signals coming from the Photon Detection Sensors of the Single Phase in the Cold Electronics side of the TPC. DAPHNE is the firs